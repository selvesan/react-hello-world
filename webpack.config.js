var webpack = require('webpack');
var path = require('path');

var BUILD_DIR = path.resolve(__dirname, 'src/client/public'); //hold's the directory path of the bundle file output.
var APP_DIR = path.resolve(__dirname, 'src/client/app'); //hold's the directory path of the React application's codebase.

var config = {
    //entry specifies the entry file using which the bundle process starts.
    //here we are instructing it to src/client/public path
    //webpack may support multiple entry points too
    entry: APP_DIR + '/index.jsx',
    //the output instructs webpack what to do after the bundling process has been completed.
    //Here we are instructing it to src/client/app directory
    output: {
        path: BUILD_DIR,
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.jsx?/,
                include: APP_DIR,
                loader: 'babel-loader'
            }
            //The loaders property takes an array of loaders, here we are just using babel-loader.
        ]
    }
};

module.exports = config;