# REACT JS HELLO WORLD APPLICATION #


![react_best_practices-1453211146748.png](https://bitbucket.org/repo/dab6bnp/images/819772189-react_best_practices-1453211146748.png)



**1) Create a New Folder**
    
```
#!php

mkdir react-hello-world
```


**2) Initialize NPM in the newly created folder**
    
```
#!php

npm init
```


**3) Installing and Configuring Webpack**
    Webpack is a module bundler which takes modules with dependencies and generates static
    assets by bundling them together based on some configuration.
    The support of loaders in Webpack makes it a perfect fit for using it along
    with React and we will discuss it later in this post with more details.

    
```
#!php

npm i webpack -S
```



**4) Create a webpack config file**
    
```
#!php

touch webpack.config.js / copy nul webpack.config.js
```

Update the webpack.config.js file with following code

    
```
#!javascript

var webpack = require('webpack');
    var path = require('path');

    var BUILD_DIR = path.resolve(__dirname, 'src/client/public'); //hold's the directory path of the bundle file output.
    var APP_DIR = path.resolve(__dirname, 'src/client/app'); //hold's the directory path of the React application's codebase.

    var config = {
        //entry specifies the entry file using which the bundle process starts.
        //here we are instructing it to src/client/public path
        //webpack may support multiple entry points too
        entry: APP_DIR + '/index.jsx',
        //the output instructs webpack what to do after the bundling process has been completed.
        //Here we are instructing it to src/client/app directory
        output: {
            path: BUILD_DIR,
            filename: 'bundle.js'
        }
    };

    module.exports = config;

```

The minimalist requirement of a Webpack config file is the presence of entry and output properties.

*     The APP_DIR holds the directory path of the React application's codebase and
*     the BUILD_DIR represents the directory path of the bundle file output.

As the name suggests, entry specifies the entry file using which the bundling process starts.
Here the index.jsx in the src/client/app directory is the starting point of the application

The output instructs Webpack what to do after the bundling process has been completed.
Here, we are instructing it to use the src/client/public directory to output the bundled file with the name bundle.js

Let's create the index.jsx file in the ./src/client/app and add the following code to verify this configuration.

    
```
#!javascript

console.log('Hello World!');
```




**5) Now start the webpack from the terminal**
   
```
#!javascript

webpack -d
```


The above command runs the webpack in the development mode and generates the bundle.js file and its associated map file bundle.js.map in the src/client/public directory.


**6) Create an index.html file in the src/client directory and modify it to use this bundle.js file**
    
```
#!html

<html>
      <head>
        <meta charset="utf-8">
        <title>React.js using NPM, Babel6 and Webpack</title>
      </head>
      <body>
        <div id="app" />
        <script src="public/bundle.js" type="text/javascript"></script>
      </body>
    </html>

```



**7) Setting up the babel-loader**
As we have seen in the beginning, by using JSX and ES6 we can be more productive while working with React.
But the JSX syntax and ES6, are not supported in all the browsers.

Hence, if we are using them in the React code, we need to use a tool which translates them to the format that has been supported by the browsers. It's where babel comes into the picture.


```
#!javascript

    npm i babel-core babel-loader babel-preset-es2015 babel-preset-react -S
```


**8) Create .babelrc**

```
#!javascript

    {
      "presets" : ["es2015", "react"]
    }

```


**9) The next step is telling Webpack to use the babel-loader while bundling the files**
   open webpack.config.js file and update it as below

   
```
#!javascript

 // Existing Code ....
    var config = {
      // Existing Code ....
      module : {
        loaders : [
          {
            test : /\.jsx?/,
            include : APP_DIR,
            loader : 'babel-loader'
          }
        ]
      }
    }

```

The loaders property takes an array of loaders, here we are just using babel-loader.


**10) Use npm to install react and react-dom**
    
```
#!javascript

npm i react react-dom -S
```

Replace the existing console.log statement in the index.jsx with the following content


```
#!javascript

    import React from 'react';
    import {render} from 'react-dom';

    class App extends React.Component {
      render () {
        return <p> Hello React!</p>;
      }
    }

    render(<App/>, document.getElementById('app'));

```

    Run the application
    webpack -d

**11) Making Webpack Watch Changes**
    
```
#!javascript

{
      // ...
      "scripts": {
        "dev": "webpack -d --watch",
        "build" : "webpack -p"
      },
      // ...
    }

```
    webpack -d --watch

Now Webpack is running in the watch mode, which will automatically bundle the file whenever there is a change detected.

If you don't like refreshing the browser to see the changes, you can use react-hot-loader!


**12) Run the NPM**
```
#!javascript

npm run
```
 build runs Webpack in production mode
    
```
#!javascript

npm run dev
```
 runs the Webpack in the watch mode.




**13) Create a new file AwesomeComponent.jsx and update it as below**
    
```
#!javascript

import React from 'react';
    class AwesomeComponent extends React.Component {

      constructor(props) {
        super(props);
        this.state = {likesCount : 0};
        this.onLike = this.onLike.bind(this);
      }

      onLike () {
        let newLikesCount = this.state.likesCount + 1;
        this.setState({likesCount: newLikesCount});
      }

      render() {
        return (
          <div>
            Likes : <span>{this.state.likesCount}</span>
            <div><button onClick={this.onLike}>Like Me</button></div>
          </div>
        );
      }

    }
    export default AwesomeComponent;

```


    Then include it in the index.jsx file

    
```
#!javascript

 import AwesomeComponent from './AwesomeComponent.jsx';
     // ...
     class App extends React.Component {
       render () {
         return (
           <div>
             <p> Hello React!</p>
             <AwesomeComponent />
           </div>
         );
       }
     }

```

[https://www.codementor.io/tamizhvendan/beginner-guide-setup-reactjs-environment-npm-babel-6-webpack-du107r9zr
](https://www.codementor.io/tamizhvendan/beginner-guide-setup-reactjs-environment-npm-babel-6-webpack-du107r9zr)

### AWESOME!!! ###